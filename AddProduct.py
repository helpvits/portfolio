import requests
import json
from Config import group_ID, inv_group_ID
import time


def UploadPhoto(api, img_size):
    print('Uploading photo')
    width = img_size[0]
    height = img_size[1]

    crop_x = width
    crop_y = height
    crop_width = width

    if width < height:
        crop_width = width
        crop_x = width
        crop_y = (height - width)/2
    if width > height:
        crop_width = height
        crop_x = (width - height)/2
        crop_y = height

    url = api.photos.getMarketUploadServer(group_id=group_ID,
                                         main_photo='1',
                                         crop_x=crop_x,
                                         crop_y=crop_y,
                                         crop_width=crop_width)
    #print('upload url = '+url['upload_url'])
    print('---uploading file ---')
    file = {'photo': ('test.jpg', open(r'test.jpg', 'rb'))}
    response=requests.post(url['upload_url'], files=file)
    resp_text=json.loads(response.text)
    print('---upload url OK ---')
    print(resp_text)
    print('---saving photo ---')
    img_id=api.photos.saveMarketPhoto(group_id=group_ID,
                                  server=resp_text['server'],
                                  photo=resp_text['photo'],
                                  hash=resp_text['hash'],
                                  crop_data=resp_text['crop_data'],
                                  crop_hash=resp_text['crop_hash'])
    print(img_id)
    print('---Saving complited ---')
    time.sleep(1)
    return img_id

def AddProduct(img_id, api, name, description, price):
    print('Add product')
    #601 мебель интерьер
    #602 Кухонные принадлежности
    print('---adding product ---')
    if len(description) < 11:
        description = description + ' Лидер продаж! '
    market_prod_ID=api.market.add(owner_id=inv_group_ID,
                     name=name,
                     description=description,
                     category_id='601',
                     price=price,
                     main_photo_id=img_id[0]['pid'])
    print(market_prod_ID)
    time.sleep(2)
    return market_prod_ID
