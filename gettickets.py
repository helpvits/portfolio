import cProfile
import functools
from concurrent.futures import ThreadPoolExecutor

from selenium import webdriver
from time import sleep
from multiprocessing.dummy import Pool as ThreadPool

options = webdriver.ChromeOptions()
options.add_argument('headless')

#pool = ThreadPool(4)

counter = 0

#  Номер билета
def get_ticket_num(ticket):
    try:
        ticket_num = ticket.find_element_by_css_selector(".ticket_id")  # Номер билета
        return ticket_num
    except:
        return False


# Чистла в билете
def get_ticket_units(ticket):
    ticket_data = set()  # Список чисел
    try:
        for item in ticket.find_elements_by_css_selector(".numbers td"):  # Чистим от пустых клеток
            if item.text != '':
                ticket_data.add(item.text)
        return ticket_data
    except:
        return False


# Запись в файл
def save_ticket(ticket_num, ticket_data, f):
    try:
        f.write("[" + str(ticket_num.text) + '],' + str(ticket_data) + "\n")
        global counter
        counter += 1
        print('TICKET SAVED ' + str(counter))
    except:
        print('Trouble written')


# Парсинг билета
def parce_ticket(ticket, f):
    ticket_num = get_ticket_num(ticket)
    if ticket_num:
        ticket_data = get_ticket_units(ticket)
        if ticket_data:
            save_ticket(ticket_num, ticket_data, f)


# Парсинг билетов
def get_tickets(driver):
    try:
        driver.get("http://www.stoloto.ru/ruslotto/game?int=right&draw=1212")  # Ссылка на лото
        print('Loto loaded')
    except:
        print('trouble driver get url')

    try:
        tickets = driver.find_elements_by_css_selector('.bingo_ticket')  # Билеты целиком
        print('Tickets found')
        f = open('numbers.txt', 'a')
        parcer = functools.partial(parce_ticket, f=f)
        with ThreadPoolExecutor() as executor:
            # with ProcessPoolExecutor(max_workers=4) as executor:
             executor.map(parcer, tickets)
        f.close()
    except:
        print('trouble find tickets')

    sleep(1)

    try:
        get_tickets(driver)
    except:
        print('trouble recurse get tickets')
        get_tickets(driver)


def make_pool():
    try:
        driver1 = webdriver.Chrome(chrome_options=options)
        driver2 = webdriver.Chrome(chrome_options=options)
        driver3 = webdriver.Chrome(chrome_options=options)
        driver4 = webdriver.Chrome(chrome_options=options)
        drivers = [driver1, driver2, driver3, driver4]

        with ThreadPoolExecutor() as executor:
            # with ProcessPoolExecutor(max_workers=4) as executor:
            executor.map(get_tickets, drivers)

        driver1.close()
        driver2.close()
        driver3.close()
        driver4.close()

    except:
        print('trouble pool')
        try:
            driver1.close()
            driver2.close()
            driver3.close()
            driver4.close()
        except:
            print('No drivers to close')


if __name__ == '__main__':
    #freeze_support()
    make_pool()


    # python -m cProfile -s time gettickets.py 10001