# -*- coding: utf-8 -*-

from tkinter import *
import os


def shutdowner(timer):
    if isinstance(timer, int):
        std_time = timer * 60
        os.system("shutdown -s -t " + str(std_time))
    elif timer == 'abort':
        os.system("shutdown /a")
    else:
        print("wrong data")


def get_time(ev):
    try:
        timer = int(time_field.get())
        shutdowner(timer)
    except:
        print("wrong timer")


def std_abort(ev):
    shutdowner('abort')

root = Tk()
root.title('Вырубала 3000')
root.geometry('200x50+300+200') # ширина=500, высота=400, x=300, y=200

time_field = Entry()
submit = Button(root, text='Вырубать')
abort = Button(root, text='Отмена')

submit.bind("<Button-1>", get_time)
abort.bind("<Button-1>", std_abort)

time_field.pack(fill='both')
submit.pack(side='left')
abort.pack(side='right')

root.mainloop()
