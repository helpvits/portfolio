import ast
from assets.AdGroups import adgroups_get
from assets.Bids import bids_get
from assets.Campaigns import campaigns_get
from assets.DataBase import get_actual_task, get_token, get_conn, del_task_by_id, get_campaigns_by_acc, add_task
from assets.KeywordBids import keywords_bids_get, keywords_bids_set
from assets.Keywords import keywords_get
from assets.common import bids_controller, keyword_bids_controller


# Типы задач:
# upd_camp => CampaignID
# upd_group => GroupID
# upd_keywords = {campaign||group:id}
# upd_bids = {wod:bid}
# upd_keyword_bids
# upgrade_keyword_bids
# upgrade_bids


# Обработка задач из таблички Tasks
def task_combine():
    tasks = get_actual_task()

    for task in tasks:
        print("Выполняем задачу: ")
        print(task)
        Id = task[0]
        account = task[1]               # Акаунт для задачи
        task_type = task[2]             # Тип задачи
        args = task[3]                  # Аргументы для задачи
        args = ast.literal_eval(args)   # Из базы пришла строка - перегнали в словарь

        token_data = get_token(account)
        token = token_data[0]  # Токен для акаунта
        login = token_data[1]  # Логин для акаунта

        if task_type == 'upd_camp':         # Компании Direct => DB
            # type_criteria = args['type_criteria']
            # criteria_data = args['criteria_data']
            campaigns_get(SelectionCriteria={}, Account=account, login=login, token=token)  # Передать token и login
            print('upd_camp')

        elif task_type == 'upd_group':      # Группы Direct => DB
            type_criteria = args['type_criteria']
            criteria_data = args['criteria_data']
            adgroups_get(type_criteria, criteria_data, login, token)
            print('upd_group')

        elif task_type == 'upd_keywords':   # Ключевые слова Direct => DB
            type_criteria = args['type_criteria']
            criteria_data = args['criteria_data']
            keywords_get(type_criteria, criteria_data, login, token)
            print('upd_keywords')

        elif task_type == 'upd_bids':       # !!! DEPRECATED !!! Ставки Direct => DB (СТАРЫЕ)
            type_criteria = args['type_criteria']
            criteria_data = args['criteria_data']
            bids = bids_get(type_criteria, criteria_data, login, token)
            conn = get_conn()
            c = conn.cursor()
            for bid in bids:
                print(bid)
                bids_controller(KeywordId=bid['KeywordId'], AdGroupId=bid['AdGroupId'], CampaignId=bid['CampaignId'],
                                Bid=bid['Bid'], P11_Bid=bid['P11_Bid'], P11_Price=bid['P11_Price'], P12_Bid=bid['P12_Bid'],
                                P12_Price=bid['P12_Price'], P13_Bid=bid['P13_Bid'], P13_Price=bid['P13_Price'], coursor_for_add=c)
            conn.commit()
            conn.close()
            print('upd_bids')

        elif task_type == 'upd_keyword_bids':   # Ставки Direct => DB (Новые)
            type_criteria = args['type_criteria']
            criteria_data = args['criteria_data']
            bids = keywords_bids_get(type_criteria, criteria_data, login, token)
            conn = get_conn()
            c = conn.cursor()
            for bid in bids:
                print(bid)
                keyword_bids_controller(KeywordId=bid['KeywordId'], AdGroupId=bid['AdGroupId'], CampaignId=bid['CampaignId'],
                                        Bid=bid['Bid'], bids_array=bid['KeywordBids'], coursor_for_add=c)
            conn.commit()
            conn.close()
            print('upd_keyword_bids')

        # !!! DEPRECATED !!! Залить ставки в директ
        # elif task_type == 'upgrade_bids':  # Ставки DB => Direct (СТАРЫЕ)
        #     bids = args['bids']
        #     bids_set(bids, login, token)  # Передать token и login
        #     print('bids_upgrade')

        elif task_type == 'upgrade_keyword_bids':   # Ставки DB => Direct (НОВЫЕ)
            bids = args['bids']
            keywords_bids_set(bids, login, token)
            print('keyword_bids_upgrade')

        else:
            print('Не правильный тип задаи = ' + task_type)

        del_task_by_id(Id)


# Создает задачу на обновление ставок для определенной группы
def make_task_to_upgrade_bids(account):
    token, login = get_token(account)
    conn = get_conn()
    c = conn.cursor()

    new_bids = []                                   # Массив для отправки ставок в директ
    campaigns = get_campaigns_by_acc(account)       # Активные компании акаунта
    for campaign in campaigns:                      # Для каждой компании получаем список ставок для слов
        bids = keywords_bids_get(type_criteria="CampaignIds", criteria_data=[campaign[0]], login=login, token=token)
        if bids:
            for bid in bids:                            # Для каждого слова провеяем ставку
                new_bid = keyword_bids_controller(KeywordId=bid['KeywordId'], AdGroupId=bid['AdGroupId'],
                                                  CampaignId=bid['CampaignId'], Bid=bid['Bid'],
                                                  bids_array=bid['KeywordBids'], coursor_for_add=c)

                if new_bid:                             # Если ствка поменялась => в массив для Директа ее
                    new_bids.append(new_bid)
        else:
            print('!!! Err не получены ставки с директа Tasks.py", line 111 NoneType')
            print('Компания: ', campaign)
    conn.commit()
    conn.close()

    add_task(account, 'upgrade_keyword_bids', "{'bids': " + str(new_bids) + "}", '0')  # Создаем задачу
