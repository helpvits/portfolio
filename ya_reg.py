from selenium import webdriver
from time import sleep
from PIL import Image
import requests
from selenium.webdriver.common.action_chains import ActionChains

# Данные пользователя
firstname = 'Василий'
lastname = 'Василич'
password = 'ОченьСложныйПарольЧтобНиктоНикогдаЕгоНеВзломалНуИСЦифрамиКонечно123ИЗнаками!№$'
hint_answer = 'НуАОтветМожноИПоПроще'


# Imitate mouse moving
def mouse_move_to(driver, elem):
    ActionChains(driver).move_to_element(elem).click(elem).perform()


options = webdriver.ChromeOptions()
# options.add_argument('headless')
driver = webdriver.Chrome(chrome_options=options)
# driver = webdriver.Firefox()
driver.implicitly_wait(5)

# Открываем яндекс и жмем завести почту
driver.get("https://passport.yandex.ru/registration/")
#reg_link = driver.find_element_by_link_text("Завести почту")
#reg_link.click()
sleep(1)

# Имя, фамилия
elem = driver.find_element_by_id('firstname')
elem.send_keys(firstname)
elem = driver.find_element_by_id('lastname')
elem.send_keys(lastname)

# Клик по логину
elem = driver.find_element_by_id('login')
mouse_move_to(driver, elem)
sleep(0.8)

# Выбираем первый вариант из предложеных свободных логинов
login = driver.find_element_by_class_name('registration__pseudo-link')
mouse_move_to(driver, login)
sleep(0.8)
print(elem.get_attribute('value'))

# Вводим и подтверждаем пароль
elem = driver.find_element_by_id('password')
elem.send_keys(password)
sleep(0.2)
elem = driver.find_element_by_name('password_confirm')
elem.send_keys(password)
sleep(0.5)

# Выбираем без телефона
elem = driver.find_element_by_class_name("link_has-no-phone")
elem.click()
sleep(1)

# Ответ на секретный вопрос
elem = driver.find_element_by_css_selector('#hint_answer.textinput__control')
elem.send_keys(hint_answer)
sleep(0.2)

# Капча
pic = driver.find_element_by_class_name('captcha__image')
cap_url = pic.get_attribute("src")
print(cap_url)

r = requests.get(cap_url, stream=True)
r.raise_for_status()
r.raw.decode_content = True  # Required to decompress gzip/deflate compressed responses.
with Image.open(r.raw) as img:
    img.show()
r.close()

captcha = input()

elem = driver.find_element_by_id('captcha')
elem.send_keys(captcha)
sleep(0.8)

# Завершаем регу
elem = driver.find_element_by_css_selector(".form__submit button")
elem.click()
sleep(7)

input()
